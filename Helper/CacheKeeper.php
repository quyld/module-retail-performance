<?php
/**
 * Created by KhoiLe - mr.vjcspy@gmail.com
 * Date: 5/15/17
 * Time: 3:51 PM
 */

namespace SM\Performance\Helper;

use Magento\Framework\ObjectManagerInterface;


/**
 * Class CacheKeeper
 *
 * @package SM\Performance\Helper
 */
class CacheKeeper {

    private $_cachedInstance = [];
    /**
     * @var \SM\Performance\Model\ResourceModel\ProductCacheInstance\CollectionFactory
     */
    protected $productCacheInstanceCollectionFactory;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var \SM\Performance\Model\ProductCacheInstanceFactory
     */
    private $productCacheInstanceFactory;

    /**
     * CacheKeeper constructor.
     *
     * @param \SM\Performance\Model\ResourceModel\ProductCacheInstance\CollectionFactory $productCacheInstanceCollectionFactory
     * @param \Magento\Framework\ObjectManagerInterface                                  $objectManager
     */
    public function __construct(
        \SM\Performance\Model\ResourceModel\ProductCacheInstance\CollectionFactory $productCacheInstanceCollectionFactory,
        \SM\Performance\Model\ProductCacheInstanceFactory $productCacheInstanceFactory,
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager                         = $objectManager;
        $this->productCacheInstanceCollectionFactory = $productCacheInstanceCollectionFactory;
        $this->productCacheInstanceFactory           = $productCacheInstanceFactory;
    }

    static $CACHE_INSTANCES
        = [
            [
                'instance' => 'SM\Performance\Model\ProductCache1'
            ],
            [
                'instance' => 'SM\Performance\Model\ProductCache2'
            ],
            [
                'instance' => 'SM\Performance\Model\ProductCache3'
            ],
            [
                'instance' => 'SM\Performance\Model\ProductCache4'
            ],
            [
                'instance' => 'SM\Performance\Model\ProductCache5'
            ]

        ];

    /**
     * @param      $storeId
     * @param null $warehouseId
     *
     * @return string|null
     */
    public function getInstance($storeId, $warehouseId = null) {
        $instanceInfo = $this->getCacheInstanceInfo($storeId, $warehouseId);
        if ($instanceInfo) {
            return $instanceInfo->getData('instance');
        }
        else {
            // get all instance
            $instances = $this->getProductCacheInstanceCollection()->toArray()['items'];
            if (count(CacheKeeper::$CACHE_INSTANCES) == count($instances)) {
                return null;
            }
            else {
                $passInstance = $this->_getInstance($instances);
                if ($passInstance) {
                    $_m = $this->getProductCacheInstanceModel();
                    $_m->setData('instance', $passInstance)
                       ->setData('is_over', 0)
                       ->setData('store_id', $storeId)
                       ->setData('warehouse_id', $warehouseId)
                       ->save();

                    return $passInstance;
                }

                return null;
            }
        }
    }

    /**
     * @param $storeId
     * @param $warehouseId
     *
     * @return \SM\Performance\Model\ProductCacheInstance|null
     */
    public function getCacheInstanceInfo($storeId, $warehouseId = null) {
        $cacheKey = $this->getCacheKey([$storeId, $warehouseId]);
        if (!isset($this->_cachedInstance[$cacheKey])) {
            $collection = $this->getProductCacheInstanceCollection();

            $collection->addFieldToFilter('store_id', $storeId);

            if (!is_null($warehouseId)) {
                $collection->addFieldToFilter('warehouse_id', $warehouseId);
            }

            $instanceInfo = $collection->getFirstItem();

            if ($instanceInfo->getId()) {
                $this->_cachedInstance[$cacheKey] = $instanceInfo;
            }
            else {
                $this->_cachedInstance[$cacheKey] = null;
            }
        }

        return $this->_cachedInstance[$cacheKey];
    }

    /**
     * @param $existedInstance
     *
     * @return mixed|null
     */
    private function _getInstance($existedInstance) {
        $allSupportInstances = array_reduce(
            CacheKeeper::$CACHE_INSTANCES,
            function ($carri, $cacheInstance) {
                $carri[] = $cacheInstance['instance'];

                return $carri;
            },
            []);

        $listExistedInstance = array_reduce(
            $existedInstance,
            function ($list, $item) {
                $list[] = $item['instance'];

                return $list;
            },
            []);

        $_i = array_diff($allSupportInstances, $listExistedInstance);
        if (count($_i) > 0) {
            return current($_i);
        }
        else
            return null;
    }

    /**
     * @return \SM\Performance\Model\ResourceModel\ProductCacheInstance\Collection
     */
    protected function getProductCacheInstanceCollection() {
        return $this->productCacheInstanceCollectionFactory->create();
    }

    /**
     * @return \SM\Performance\Model\ProductCacheInstance
     */
    protected function getProductCacheInstanceModel() {
        return $this->productCacheInstanceFactory->create();
    }

    public function getCacheKey($arrayKey) {
        return array_reduce(
            $arrayKey,
            function ($carry, $item) {
                $carry .= $item . "|";

                return $carry;
            },
            "");
    }

    static function getCacheTime() {
        return intval(microtime(true));
    }
}