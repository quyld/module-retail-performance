<?php
namespace SM\Performance\Api;

use SM\Performance\Api\Data\ProductCache5Interface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductCache5RepositoryInterface 
{
    public function save(ProductCache5Interface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ProductCache5Interface $page);

    public function deleteById($id);
}
