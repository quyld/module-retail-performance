<?php
namespace SM\Performance\Api;

use SM\Performance\Api\Data\ProductCache4Interface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductCache4RepositoryInterface 
{
    public function save(ProductCache4Interface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ProductCache4Interface $page);

    public function deleteById($id);
}
