<?php
namespace SM\Performance\Api;

use SM\Performance\Api\Data\ProductCache3Interface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductCache3RepositoryInterface 
{
    public function save(ProductCache3Interface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ProductCache3Interface $page);

    public function deleteById($id);
}
