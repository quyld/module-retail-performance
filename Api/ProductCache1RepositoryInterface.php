<?php
namespace SM\Performance\Api;

use SM\Performance\Api\Data\ProductCache1Interface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductCache1RepositoryInterface 
{
    public function save(ProductCache1Interface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ProductCache1Interface $page);

    public function deleteById($id);
}
