<?php
namespace SM\Performance\Api;

use SM\Performance\Api\Data\ProductCache2Interface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductCache2RepositoryInterface 
{
    public function save(ProductCache2Interface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ProductCache2Interface $page);

    public function deleteById($id);
}
