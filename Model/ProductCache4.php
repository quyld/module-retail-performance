<?php

namespace SM\Performance\Model;

class ProductCache4 extends AbstractProductCache
    implements \SM\Performance\Api\Data\ProductCache4Interface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_performance_productcache4';

    protected function _construct() {
        $this->_init('SM\Performance\Model\ResourceModel\ProductCache4');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
