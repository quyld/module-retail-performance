<?php

namespace SM\Performance\Model;

class ProductCache3 extends AbstractProductCache
    implements \SM\Performance\Api\Data\ProductCache3Interface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_performance_productcache3';

    protected function _construct() {
        $this->_init('SM\Performance\Model\ResourceModel\ProductCache3');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
