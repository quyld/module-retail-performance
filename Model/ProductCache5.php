<?php

namespace SM\Performance\Model;

class ProductCache5 extends AbstractProductCache
    implements \SM\Performance\Api\Data\ProductCache5Interface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_performance_productcache5';

    protected function _construct() {
        $this->_init('SM\Performance\Model\ResourceModel\ProductCache5');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
