<?php

namespace SM\Performance\Model\ResourceModel;

class ProductCache2 extends AbstractProductCache {

    protected function _construct() {
        $this->_init('sm_performance_product_cache2', 'id');
        $this->_isPkAutoIncrement = false;
    }
}
