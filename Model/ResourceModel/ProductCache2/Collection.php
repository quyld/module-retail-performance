<?php
namespace SM\Performance\Model\ResourceModel\ProductCache2;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('SM\Performance\Model\ProductCache2','SM\Performance\Model\ResourceModel\ProductCache2');
    }
}
