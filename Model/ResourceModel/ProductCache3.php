<?php

namespace SM\Performance\Model\ResourceModel;

class ProductCache3 extends AbstractProductCache {

    protected function _construct() {
        $this->_init('sm_performance_product_cache3', 'id');
        $this->_isPkAutoIncrement = false;
    }
}
