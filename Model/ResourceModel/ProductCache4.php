<?php

namespace SM\Performance\Model\ResourceModel;

class ProductCache4 extends AbstractProductCache {

    protected function _construct() {
        $this->_init('sm_performance_product_cache4', 'id');
        $this->_isPkAutoIncrement = false;
    }
}
