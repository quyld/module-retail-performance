<?php
namespace SM\Performance\Model\ResourceModel\ProductCache4;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('SM\Performance\Model\ProductCache4','SM\Performance\Model\ResourceModel\ProductCache4');
    }
}
