<?php

namespace SM\Performance\Model\ResourceModel;

class ProductCache1 extends AbstractProductCache {

    protected function _construct() {
        $this->_init('sm_performance_product_cache1', 'id');
    }
}
