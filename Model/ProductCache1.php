<?php

namespace SM\Performance\Model;

class ProductCache1 extends AbstractProductCache
    implements \SM\Performance\Api\Data\ProductCache1Interface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_performance_productcache1';

    protected function _construct() {
        $this->_init('SM\Performance\Model\ResourceModel\ProductCache1');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
