<?php

namespace SM\Performance\Model;

class ProductCache2 extends AbstractProductCache
    implements \SM\Performance\Api\Data\ProductCache2Interface, \Magento\Framework\DataObject\IdentityInterface {

    const CACHE_TAG = 'sm_performance_productcache2';

    protected function _construct() {
        $this->_init('SM\Performance\Model\ResourceModel\ProductCache2');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
