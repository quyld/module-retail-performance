<?php
/**
 * Created by KhoiLe - mr.vjcspy@gmail.com
 * Date: 5/16/17
 * Time: 3:19 PM
 */

namespace SM\Performance\Observer\ProductCaching;


use Magento\Framework\Event\Observer;
use Magento\Framework\ObjectManagerInterface;
use SM\Performance\Helper\CacheKeeper;

class AfterLoading implements \Magento\Framework\Event\ObserverInterface {

    /**
     * @var \SM\Performance\Helper\CacheKeeper
     */
    protected $cacheKeeper;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(\SM\Performance\Helper\CacheKeeper $cacheKeeper, ObjectManagerInterface $objectManager) {
        $this->cacheKeeper   = $cacheKeeper;
        $this->objectManager = $objectManager;
    }

    /**
     * Nếu không sử dụng cache và loading bình thường thì cân lưu vào cache
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        // TODO: Implement execute() method.
        $loadingData = $observer->getData('loading_data');
        /** @var \Magento\Framework\DataObject $searchCriteria */
        $searchCriteria = $loadingData->getData('search_criteria');
        $storeId        = $searchCriteria->getData('storeId');
        $warehouseId    = $searchCriteria->getData('warehouse_id');

        $cacheInstance = $this->cacheKeeper->getInstance($storeId, $warehouseId);
        $cacheInfo     = $this->cacheKeeper->getCacheInstanceInfo($storeId, $warehouseId);

        if ($cacheInstance && $loadingData->getData('is_use_cache') !== true) {
            $items = $loadingData->getData('items');
            /** @var \SM\Core\Api\Data\XProduct[] $items */
            foreach ($items as $item) {
                /** @var \SM\Performance\Model\AbstractProductCache $model */
                $model = $this->objectManager->create($cacheInstance);
                $model->setData('id', $item->getId())->setData('data', json_encode($item->getData()))->save();
            }
        }

        $cacheInfo->setData('cache_time', CacheKeeper::getCacheTime());

        if ($loadingData->getData('is_full_loading') === true) {
            $cacheInfo->setData('is_over', true);
        }

        $cacheInfo->save();
    }

}