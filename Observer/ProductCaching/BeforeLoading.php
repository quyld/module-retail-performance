<?php
/**
 * Created by KhoiLe - mr.vjcspy@gmail.com
 * Date: 5/16/17
 * Time: 3:19 PM
 */

namespace SM\Performance\Observer\ProductCaching;

use Magento\Framework\ObjectManagerInterface;
use SM\Core\Api\Data\XProduct;

class BeforeLoading implements \Magento\Framework\Event\ObserverInterface {

    protected $useCache = true;
    /**
     * @var \SM\Performance\Helper\CacheKeeper
     */
    protected $cacheKeeper;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(\SM\Performance\Helper\CacheKeeper $cacheKeeper, ObjectManagerInterface $objectManager) {
        $this->cacheKeeper   = $cacheKeeper;
        $this->objectManager = $objectManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $loadingData = $observer->getData('loading_data');
        /** @var \Magento\Framework\DataObject $searchCriteria */
        $searchCriteria = $loadingData->getData('search_criteria');
        $storeId        = $searchCriteria->getData('storeId');
        $warehouseId    = $searchCriteria->getData('warehouse_id');

        $cacheInfo = $this->cacheKeeper->getCacheInstanceInfo($storeId, $warehouseId);

        if (!$cacheInfo || !$this->useCache) {
            // Max cache
            return;
        }

        $isRealTime = floatval($searchCriteria->getData('realTime')) == 1;
        $cacheTime  = $searchCriteria->getData('cache_time');

        if ($isRealTime) {
            if (!$cacheTime || is_nan($cacheTime)) {
                throw new \Exception("Realtime must have param cache_time and cache time must be number");
            }

            if (floatval($cacheInfo->getData('cache_time')) < floatval($cacheTime) || boolval($cacheInfo->getData('is_over')) !== true) {
                return;
            }
            else {
                /** @var \SM\Performance\Model\AbstractProductCache $cacheInstance */
                $cacheInstance = $this->objectManager->create($cacheInfo->getData('instance'));
                $collection    = $cacheInstance->getCollection();

                if ($searchCriteria->getData('entity_id') || $searchCriteria->getData('entityId')) {
                    $ids = is_null($searchCriteria->getData('entity_id'))
                        ? $searchCriteria->getData('entityId')
                        : $searchCriteria->getData(
                            'entity_id');
                    $collection->addFieldToFilter('id', ['in' => explode(",", $ids)]);
                }
                $loadingData->setData('is_use_cache', true);
                $loadingData->setData('collection', $collection);
                $loadingData->setData('items', $this->retrieveDataFromCollection($collection));
            }
        }
        else if ($cacheInfo && boolval($cacheInfo->getData('is_over')) === true) {
            /** @var \SM\Performance\Model\AbstractProductCache $cacheInstance */
            $cacheInstance = $this->objectManager->create($cacheInfo->getData('instance'));

            if (!$cacheInstance) {
                throw new \Exception("Error SM\\Performance\\Observer\\ProductCaching\\BeforeLoading");
            }

            $collection  = $cacheInstance->getCollection();
            $currentPage = is_nan($searchCriteria->getData('currentPage')) ? 1 : $searchCriteria->getData('currentPage');
            $collection->setCurPage($currentPage);
            if ($searchCriteria->getData('productIds')) {
                $collection->addFieldToFilter('id', ['in' => $searchCriteria->getData('productIds')]);
            }
            if ($searchCriteria->getData('entity_id') || $searchCriteria->getData('entityId')) {
                $ids = is_null($searchCriteria->getData('entity_id')) ? $searchCriteria->getData('entityId') : $searchCriteria->getData('entity_id');
                $collection->addFieldToFilter('id', ['in' => explode(",", $ids)]);
            }
            $collection->setPageSize(5000);
            $loadingData->setData('is_use_cache', true);
            $loadingData->setData('collection', $collection);

            if ($collection->getLastPageNumber() < $currentPage) {
                $loadingData->setData('items', []);
            }
            else {
                $loadingData->setData('items', $this->retrieveDataFromCollection($collection));
            }
        }
    }

    protected function retrieveDataFromCollection($collection) {
        $items = [];
        foreach ($collection as $item) {
            $itemData = json_decode($item->getData('data'), true);
            if (is_array($itemData)) {
                $xProduct = new XProduct($itemData);
                $items[]  = $xProduct;
            }
        }

        return $items;
    }
}