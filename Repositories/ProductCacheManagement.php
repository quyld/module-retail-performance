<?php

namespace SM\Performance\Repositories;

use Magento\Framework\ObjectManagerInterface;
use SM\XRetail\Repositories\Contract\ServiceAbstract;
use Magento\Framework\DataObject;

class ProductCacheManagement extends ServiceAbstract {

    /**
     * @var \SM\Performance\Model\ProductCacheInstanceFactory
     */
    protected $productCacheInstanceFactory;

    /**
     * @var \SM\Performance\Model\ResourceModel\ProductCacheInstance\CollectionFactory
     */
    protected $productCacheInstanceCollectionFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \SM\XRetail\Helper\DataConfig $dataConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ObjectManagerInterface $objectManager,
        \SM\Performance\Model\ProductCacheInstanceFactory $productCacheInstanceFactory,
        \SM\Performance\Model\ResourceModel\ProductCacheInstance\CollectionFactory $productCacheInstanceCollectionFactory
    ) {
        $this->objectManager                         = $objectManager;
        $this->productCacheInstanceFactory           = $productCacheInstanceFactory;
        $this->productCacheInstanceCollectionFactory = $productCacheInstanceCollectionFactory;
        parent::__construct($requestInterface, $dataConfig, $storeManager);
    }

    public function getListProductCache() {
        $searchCriteria = $this->getSearchCriteria();

        return $this->getProductCache($searchCriteria);
    }

    public function getProductCache(DataObject $searchCriteria) {
        $collection = $this->productCacheInstanceCollectionFactory->create();
        $items      = [];
        if ($collection->getLastPageNumber() < $searchCriteria->getData('currentPage')) {
        }
        else {
            foreach ($collection as $productCache) {
                $productCacheData = $productCache->getData();
                $items[]          = $productCacheData;
            }
        }

        return $this->getSearchResult()
                    ->setSearchCriteria($searchCriteria)
                    ->setItems($items)
                    ->setLastPageNumber($collection->getLastPageNumber())
                    ->getOutput();
    }

    public function removeProductCache() {
        $id = $this->getRequest()->getParam('id');
        if (is_null($id)) {
            throw new \Exception("Must define required data");
        }
        $productCacheItem = $this->productCacheInstanceFactory->create()->load($id);
        if (!$productCacheItem->getId()) {
            throw new \Exception("Can not find product cache data");
        }
        else {
            $cacheInstance = $productCacheItem->getData('instance');
            $tableInstance = $this->objectManager->create($cacheInstance)->getCollection();
            $connection    = $tableInstance->getConnection();
            $connection->truncateTable($tableInstance->getMainTable());
            $productCacheItem->delete();
        }

        return ['messages' => 'Product data has been deleted'];
    }


}
